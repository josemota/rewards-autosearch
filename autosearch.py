from selenium import webdriver
from selenium.webdriver.chrome.options import Options
import time, requests, random
from sys import exit
import config

from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC


driver = webdriver.Chrome()

def configure(platform):
    chrome_options = Options()
    chrome_options.add_argument("webdriver.chrome.driver=/usr/local/bin/chromedriver-linux64/chromedriver")
    # chrome_options.add_argument("--incognito")

    edge_desktop_emulation = {
        "userAgent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.61 Safari/537.36 Edg/83.0.478.37"}
    edge_mobile_emulation = {
        "deviceMetrics": {"width": 360, "height": 640, "pixelRatio": 3.0},
        # "userAgent": "Mozilla/5.0 (Windows Phone 10.0; Android 6.0.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.102 Mobile Safari/537.36 Edge/18.19041"}
        # "userAgent": "Mozilla/5.0 (Windows Phone 10.0; Android 4.2.1; WebView/3.0) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/64.118.222 Chrome/52.0.2743.116 Mobile Safari/537.36 Edge/15.15063"}
        "userAgent": "Mozilla/5.0 (Linux; Android 12; SM-N9750) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Mobile Safari/537.36 EdgA/107.0.1418.28"}
        # "userAgent": "Mozilla/5.0 (Linux; Android 4.2.1; en-us; Nexus 5 Build/JOP40D) AppleWebKit/535.19 (KHTML, like Gecko) Chrome/18.0.1025.166 Mobile Safari/535.19"}
    

    match platform:
        case "desktop":
            chrome_options.add_experimental_option("mobileEmulation", edge_desktop_emulation)
        case "mobile":
            chrome_options.add_experimental_option("mobileEmulation", edge_mobile_emulation)
        case _:
            print("unknown platform. must be desktop or mobile.")
            exit(1)
            
    global driver 
    driver = webdriver.Chrome(options=chrome_options)

def login(email, password):
    driver.get("https://login.live.com/login.srf?wa=wsignin1.0&wreply=https%3a%2f%2fwww.bing.com")

    email_field = WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.XPATH, '//input[@type="email"][@name="loginfmt"][@id="i0116"]')))
    email_field.send_keys(email)

    next = WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.XPATH, '//*[@type="submit"][@id="idSIButton9"]')))
    next.click()

    password_field = WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.XPATH, '//input[@type="password"][@name="passwd"][@id="i0118"]')))
    password_field.send_keys(password)

    signin = WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.XPATH, '//*[@type="submit"][@id="idSIButton9"]')))
    signin.click()
    time.sleep(60)

    while True:
        try:
            stay_signed = WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.XPATH, '//*[@type="submit"][@id="idSIButton9"]')))
            stay_signed.click()
            break
        except Exception as e:
            driver.refresh()


def logout():
    driver.get("https://account.microsoft.com/?refd=www.bing.com")

    profile_pic = WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.XPATH, '//button[@id="O365_MainLink_Me"]')))
    profile_pic.click()

    logout = WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.XPATH, '//a[@id="mectrl_body_signOut"]')))
    logout.click()

def search(keywords):
    driver.get('https://www.bing.com/')
    for word in keywords:
        searchbar = WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.XPATH, '//*[@id="sb_form_q"]')))
        searchbar.clear()
        time.sleep(random.uniform(7, 14))
        searchbar.send_keys(word)
        searchbar.submit()

def get_word_list(lang, count):
    # RANDOM WORDS API ENDPOINT
    api_url = "https://random-word-api.herokuapp.com/word"
    # Parameters for the API call
    params = {"lang": lang, "number": count}
    response = requests.get(api_url, params=params)

    # Check if the request was successful (status code 200)
    if response.status_code == 200:
        # Parse the JSON response
        word_list = response.json()
        return word_list
    else:
        print(f"Error: {response.status_code}")
        exit(1)

def main():
    lang = config.lang
    desktop_count = config.desktop_count
    mobile_count = config.mobile_count

    for user in config.credentials:  
        if desktop_count > 0:      
            configure("desktop")
            login(user['user'], user['pass'])
            search(get_word_list(lang, desktop_count))
            # logout()

        if mobile_count > 0:
            configure("mobile")
            login(user['user'], user['pass'])
            search(get_word_list(lang, mobile_count))
            # logout()

        driver.quit()



if __name__ == "__main__":
    main()