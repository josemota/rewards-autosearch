[![License](https://img.shields.io/badge/License-CC%20BY%204.0-lightgrey.svg)](https://creativecommons.org/licenses/by/4.0/)
[![Python Version](https://img.shields.io/badge/Python-3.10-blue?style=flat&logo=python&logoColor=white&label=Python&color=blue)](https://www.python.org/downloads/release/python-310/)
![GitLab last commit](https://img.shields.io/gitlab/last-commit/josemota%2Frewards-autosearch?logo=gitlab)
![Version](https://img.shields.io/gitlab/v/tag/josemota%2Frewards-autosearch?logo=git&logoColor=white&label=version)
![GitLab stars](https://img.shields.io/gitlab/stars/josemota%2Frewards-autosearch)

# Bing Automation Script for Microsoft Rewards

## Overview

This Python script automates Bing searches to simplify Microsoft Rewards tasks and help users earn points more efficiently.

**Disclaimer:** The creator of this script is not responsible for any issues, including account bans or penalties, that may arise from the use of this tool.

## Prerequisites

Before using the script, ensure you have the following installed:

1. [Python 3.10](https://www.python.org/downloads/release/python-310/)
2. [pip3](https://pip.pypa.io/en/stable/installation/)

## Installation

1. Clone or download the project from the repository.
2. Open a terminal and navigate to the project directory.

### Install Python and pip

- For Ubuntu:

    ```bash
    sudo apt update
    sudo apt install python3
    sudo apt install python3-pip
    ```

### Install Required Libraries

```bash
pip3 install -r requirements.txt
```

## Chrome WebDriver Setup (for Ubuntu x64)

1. Download the [ChromeDriver](https://sites.google.com/chromium.org/driver/downloads) suitable for your Chrome version and system architecture.
2. Extract the downloaded archive.
3. Copy the `chromedriver` executable to a directory in your system's PATH (e.g., `/usr/local/bin/`).

## Configuration

Modify the `config.py` file with your preferences:

- `lang`: Language for search queries.
- `desktop_search_count`: Number of searches for desktop configuration.
- `mobile_search_count`: Number of searches for mobile configuration.
- `credentials`: List of user credentials for Microsoft login.

## Usage

Run the script:

```bash
python3 autosearch.py
```

## License

This script is licensed under the [Creative Commons Attribution License](https://creativecommons.org/licenses/by/4.0/). You are free to share and adapt the script for any purpose, even commercially, with appropriate attribution.
